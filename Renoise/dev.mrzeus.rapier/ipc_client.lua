---@diagnostic disable: lowercase-global

_M = {}

_M.get_option = function (property_name)

end

--------------client socket------------------------
ipc_client = nil

function ipc_connected()
    if ipc_client == nil then
        return false
    end

    if ipc_client.is_open == false then
        print("Changed ipc_client to nil because it was closed. - ipc_connected()")
        ipc_client = nil
    end

    return true
end
_M.ipc_connected = ipc_connected

--------------private IPC function--------------
function safe_send(message)
    if ipc_connected() then
        ipc_client:send(message .. "|")
    else
        print("Unconnected but still sent signal: " .. message)
    end
end

function send_play_signal()
    safe_send("PLAYY")
end

function send_pause_signal()
    safe_send("PAUSE")
end

function send_time_signal(time)
    safe_send("STTIM " .. tostring(time))
end

function bool_to_number(value)
    return value and 1 or 0
end

function send_record_signal(armed)
    safe_send("STREC " .. bool_to_number(armed))
end

function send_bpm_signal(bpm)
    safe_send("STBPM " .. tostring(bpm))
end

--------------private util functions------------------
-- https://stackoverflow.com/a/30960054/8062091
local library_extention = package.cpath:match("%p[\\|/]?%p(%a+)")

if library_extention == "dll" then
    function sleep(msecs)
        os.execute("ping 127.0.0.1 -n 1 -w " .. tostring(msecs) .." > nul")
    end
else
    function sleep(msecs)
        --                  To reduce to decimal for sleep (i.e. 10 to 0.010)
        --                                    vvvvvvv 
        os.execute("sleep " .. tostring(msecs / 1000))
    end
end

-------------------public IPC functions-------------
function reset()
    if ipc_connected() then
        disconnect_ipc_client()
        connect_ipc_client()
        send_play_state()
        send_recording_mode()
        send_bpm_signal(renoise.song().transport.bpm)
    end
end
_M.reset = reset

function send_new_bpm()
    send_bpm_signal(renoise.song().transport.bpm)
end
_M.send_new_bpm = send_new_bpm

function send_play_state()
    if renoise.song().transport.playing == true then
        local pos = renoise.song().transport.playback_pos
        renoise.song().transport:stop()

        send_play_signal()

        sleep(_M.get_option("latency_offset").value)

        renoise.song().transport:start_at(pos)
    else
        send_pause_signal()
    end
end
_M.send_play_state = send_play_state

function send_recording_mode()
    if renoise.song().transport.edit_mode then
        send_record_signal(true)
    else
        send_record_signal(false)
    end
end
_M.send_recording_mode = send_recording_mode


_M.send_time = send_time_signal
-----------------------------SOCKET STUFF---------------------------------
function connect_ipc_client()
    local connection_error
    ipc_client, connection_error = renoise.Socket.create_client(
        _M.get_option("reaper_hostname").value,
        _M.get_option("reaper_port").value,
        renoise.Socket.PROTOCOL_TCP
    )

    if connection_error then
        renoise.app():show_warning("Rapier failed to start IPC client to Reaper with error:\n\n" .. connection_error .. "\n\nMake sure Reaper server is running.")
        ipc_client = nil
    end
end
_M.connect_ipc_client = connect_ipc_client

function disconnect_ipc_client()
    if not ipc_connected() then
        return
    else
        ipc_client:close()
    end
end
_M.disconnect_ipc_client = disconnect_ipc_client

return _M