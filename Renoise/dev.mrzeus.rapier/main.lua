---@diagnostic disable: lowercase-global

-----------------
-- Imports
-----------------
ipc = require "ipc_client"

-----------------
-- Preferences --
-----------------
local options = renoise.Document.create("RapierPreferences") {
    reaper_hostname = "localhost",
    reaper_port = 7366,
    latency_offset = 0
}

-- Register prefs
renoise.tool().preferences = options
ipc.get_option = function (property_name)
    return options:property(property_name)
end

-------------------------------------------
-- Hooks
-------------------------------------------
hooked = false

-- IPC does fancy delay compensation which calls play
-- and pause which then calls the hook again creating
-- an infinite loop. We must compensate for this by things.
local playing_callback_lock = false

function unlock_playing_callback()
    playing_callback_lock = false
    if renoise.tool():has_timer(unlock_playing_callback) then
        renoise.tool():remove_timer(unlock_playing_callback)
    end
end

function safe_send_time()
    local time = 0
    local lpb = renoise.song().transport.lpb
    local bpm = renoise.song().transport.bpm
    local time_per_line = (bpm * lpb) / 60
    local target_seq = renoise.song().transport.playback_pos.sequence
    local target_line = renoise.song().transport.playback_pos.line

    for i = 1, target_seq, 1 do
        local pattern_length = renoise.song().patterns[i].number_of_lines
        for j = 1, pattern_length, 1 do
            if i == target_seq and j == target_line then
                ipc.send_time(time)
                return
            else
                time = time + time_per_line
            end
        end
    end
end

function safe_send_play_state()

    if not playing_callback_lock then
        if renoise.song().transport.playing == true then
            playing_callback_lock = true
            renoise.tool():add_timer(unlock_playing_callback, options:property("latency_offset").value + 300)
            safe_send_time()
            ipc.send_play_state()
        end
    end

    -- No need to lock for pausing, because there is no delay comp.
    if renoise.song().transport.playing == false then
        ipc.send_play_state()
    end
end


-- hooks = {
--     { renoise.song().transport.playing_observable, safe_send_play_state },
--     { renoise.song().transport.bpm_observable, ipc.send_new_bpm},
--     { renoise.song().transport.edit_mode_observable, ipc.send_recording_mode}
-- }

function hook()
    renoise.song().transport.playing_observable:add_notifier(safe_send_play_state)
    renoise.song().transport.bpm_observable:add_notifier(ipc.send_new_bpm)
    renoise.song().transport.edit_mode_observable:add_notifier(ipc.send_recording_mode)
    hooked = true
end

function unhook()
    renoise.song().transport.playing_observable:remove_notifier(safe_send_play_state)
    renoise.song().transport.bpm_observable:remove_notifier(ipc.send_new_bpm)
    renoise.song().transport.edit_mode_observable:remove_notifier(ipc.send_recording_mode)
    hooked = false
end

function update_hooks()
    if ipc.ipc_connected() then
        if not hooked then
            hook()
        end
    else
        if hooked then
            unhook()
        end
    end
end
-------------------------------------------
-- Menu entries
-------------------------------------------
renoise.tool():add_menu_entry {
    name = "Main Menu:Tools:rapier:Connected to Server",
    invoke = function () end,
    active = function ()
        return ipc.ipc_connected()
    end,
    selected = function ()
        return ipc.ipc_connected()
    end
}

renoise.tool():add_menu_entry {
    name = "Main Menu:Tools:rapier:Connect to Reaper",
    invoke = function ()
        ipc.connect_ipc_client()
        update_hooks()
        
    end
}

renoise.tool():add_menu_entry {
    name = "Main Menu:Tools:rapier:Disconnect to Reaper",
    invoke = function ()
        ipc.disconnect_ipc_client()
        update_hooks()
    end
}

renoise.tool():add_menu_entry {
    name = "Main Menu:Tools:rapier:Reset",
    active = function ()
        return ipc.ipc_connected()
    end,
    invoke = function ()
        ipc.reset()
        update_hooks()
    end
}



function show_latency_offet_popup()
    local vb = renoise.ViewBuilder()
    local content = vb:row {
        vb:valuebox {
            value = options.latency_offset.value,
            max = 1000,
            notifier = function (value)
                options.latency_offset.value = value
            end
        },
        vb:text {
            text = "ms"
        }
    }

    renoise.app():show_custom_prompt(
        "Set Latency Offset", content, {"OK"})
end

renoise.tool():add_menu_entry {
    name = "Main Menu:Tools:rapier:Settings:Set Latency Offset",
    invoke = function ()
        show_latency_offet_popup()
    end
}

renoise.tool():add_menu_entry {
    name = "Main Menu:Tools:rapier:Settings:List Option",
    invoke = function ()
        renoise.app():show_warning(type(ipc.get_option("reaper_hostname").value))
    end
}