# rapier

rapier is a IPC protocol that syncs transport data between Renoise and some other
backend. (Or possibly two arbitrary DAWs).

It's current self is a Renoise tool (in ./Renoise/) and a Python server in a file
that connects to Reaper via Reapy (in ./Reaper/).

# A Note:

Due to the IPC protocol being documented it shouldn't be hard to build another
backend for the Renoise client. If you want to provide another backend just
create a TCP server on the same host and port as set in the Renoise preferences
and process the signals, translating them into commands for that DAW.

I may say "Renoise" and "Reaper" at times, but the DAW is arbitrary aside
from Renoise meaning the master transport source and Reaper meaning the
slave transport source.

# Technical Note:

Renoise acts as a client to Reaper because of the technical limitations of 
Renoise's socket API. Servers can only receive data from clients in the 
callback arch of Renoise.

Renoise (client) -> Reaper (server).

# IPC Protocol

All signals are formatted like so `[code] [parameters] |`.
- A five letter code identifier
- Any number of parameters split by spaces.
- The pipe delineating the end of a signal.

### Signal list:

| code | parameters | meaning |
|---|---|---|
| `PLAYY` | none | Start playing |
| `PAUSE` | none | Pause or stop. |
| `STREC` | `[state: boolean]` | Arm record state. |
| `STBPM` | `[bpm: int]` | Set BPM. |
| `STPCS` | `[enabled: int]` | Set precount enabled/disabled. (PreCount State) |
| `STPCB` | `[num_bars: int]` | Set the number of bars of precount there are. (PreCount Bars) |
