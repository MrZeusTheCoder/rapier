#!/usr/bin/python3

import os
import zipfile

tool_version = "0.0.0"
tool_name = "rapier"

try:
    os.mkdir("package/Renoise/")
except OSError as e:
    if e.errno != 17:
        print(f"Unable to make \"package\" folder: {e}")

# XRNX files are just glorified zip files...
with zipfile.ZipFile(f"package/Renoise/{tool_name}-{tool_version}.xrnx", "w") as xrnx_file:
    for file in os.listdir("Renoise/dev.mrzeus.rapier/"):
        xrnx_file.write("Renoise/dev.mrzeus.rapier/" + file, file)

