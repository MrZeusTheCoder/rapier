#!/bin/python3
import logging
import os
import json
import warnings

from banner import banner

print(banner)
print("- rapier IPC Server for Reaper\n")


def setup_logging() -> logging.Logger:
    time_format = "%H:%M:%S"
    formatter = logging.Formatter(
        "[%(asctime)s.%(msecs)03d]:%(levelname)s -> %(message)s", time_format)
    handler = logging.StreamHandler()
    handler.setFormatter(formatter)
    logger = logging.getLogger("rapier")
    logger.setLevel(logging.INFO)
    logger.addHandler(handler)
    return logger


logger = setup_logging()
logger.info("Initialized logging.")

# Make sure Reaper is open and not on it.
with warnings.catch_warnings(record=True) as caught_warnings:
    warnings.simplefilter("always")
    import reapy
    if len(caught_warnings) > 0:
        for w in caught_warnings:
            if w.category == reapy.errors.DisabledDistAPIWarning:
                print(
                    "\n!!! Make sure Reaper is open. rapier's internals warned about being unable to access Reaper. !!!\n")

            print("Warning trace: " + str(w) + "\n")
    else:
        logger.info("Connected to Reaper.")

def load_preferences() -> str:
    preferences = {}
    if os.path.exists("./rapier_preferences.json"):
        with open("rapier_preferences.json", "r") as prefs_file:
            preferences = json.loads(prefs_file.read())
    else:
        host = input("Hostname: ")
        port = input("Port: ")
        preferences = {
            "host": host,
            "port": port,
            "debug": False
        }
    return preferences


def main(preferences):
    host = preferences["host"]
    port = preferences["port"]
    logger.info(f"Initalizing with host as \'{host}\' and port as \'{port}\'.")
    import server_internals
    server_internals.run(host, port)


if __name__ == "__main__":
    preferences = load_preferences()
    if preferences["debug"]:
        logger.setLevel(logging.DEBUG)
    main(preferences)
