import logging
from typing import List
import warnings
import socket
import mido
import rapier_midi_device as rmd
import time

with warnings.catch_warnings():
    import reapy

logger = logging.getLogger("rapier")

project = reapy.Project()
record_armed = False


def set_record_mode(params: str):
    record_armed = params[0] == "1"
    logger.debug(f"Set recording armed to {record_armed}.")



def send_midi_cc(control):
    message = mido.Message('control_change', control=control, value=100)
    rmd.device.send(message)

def process_play():
    if record_armed:
        send_midi_cc(rmd.RECORD_CONTROL)
    else:
        send_midi_cc(rmd.PLAY_CONTROL)

def process_pause():
    send_midi_cc(rmd.PAUSE_CONTROL)

def set_bpm(params: str):
    try:
        bpm = int(params)
        project.bpm = float(bpm)
    except ValueError as e:
        logger.warn(
            f"Failed to decode bpm signal parameters to integer: \"{bpm}\"")
        logger.warn(f"Full error trace: {e.with_traceback()}")

def set_time(params: str):
    project.cursor_position = float(params)

def set_precount(params: str):
    project.set

def set_precount_bars(params: str):
    pass

def process_signals(signals: List[str]):
    start_time = time.time()
    for signal in signals:
        logger.debug(f"Processing signal: \"{signal}\"")
        code = signal[0:5]
        params = signal[6:]
        if code == "PLAYY":
            process_play()
        elif code == "PAUSE":
            process_pause()
        elif code == "STREC":
            set_record_mode(params)
        elif code == "STBPM":
            set_bpm(params)
        elif code == "STPCS":
            set_precount(params)
        elif code == "STPCB":
            set_precount_bars(params)
        elif code == "STTIM":
            set_time(params)
        else:
            logger.warn(
                f"Got unrecognized signal code \"{code}\" and parameters \"{params}\". May indicate outdated or incompatible versions of this server and Renoise client.")
    logger.debug(f"Signal processing took, {time.time() - start_time} seconds.")

def run(host, port):
    logger.info("Starting server...")
    server_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server_sock.bind((host, port))
    logger.info("Started server socket.")
    try:
        server_sock.listen()
        while True:
            client, addr = server_sock.accept()
            logger.debug(f"Client connected from {addr}.")
            last_signal = ":)"
            while True:
                data = client.recv(1024).decode()
                if data == "" and last_signal == "":
                    logger.warn(
                        "Disconnected from client. Socket closed. (received two invalid signals)")
                    break

                if data != "":
                    signals = data.split("|")
                    signals = [x for x in signals if x != ""]
                    process_signals(signals)

                last_signal = ""
    except KeyboardInterrupt:
        if server_sock:
            server_sock.close()
