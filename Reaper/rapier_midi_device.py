import mido
import logging

PLAY_CONTROL = 1
PAUSE_CONTROL = 2
RECORD_CONTROL = 3

mido.set_backend("mido.backends.rtmidi/UNIX_JACK")
device = mido.open_output("rapier Reaper Server Transport Device", virtual=True)

logger = logging.getLogger("rapier")
logger.info("Successfully opened Mido MIDI device for transport controls.")