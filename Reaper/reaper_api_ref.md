## Logging

`reaper.ShowConsoleMsg("Hello, World!")`

## Time Stuff

### TimeMap2_beatsToTime

Lua: number reaper.TimeMap2_beatsToTime(ReaProject proj, number tpos, optional number measuresIn)

convert a beat position (or optionally a beats+measures if measures is non-NULL) to time.

## Play State

Lua: `integer reaper.GetPlayState()`
&1=playing, &2=pause, &=4 is recording

Lua: `reaper.CSurf_SetPlayState(boolean play, boolean pause, boolean rec, IReaperControlSurface ignoresurf)`

## Cursor

Lua: `reaper.SetEditCurPos(number time, boolean moveview, boolean seekplay)`
time: Set new time position edit cursor
moveview: Move view if it's outside the current screen (seekplay should be set to false)
seekplay: Set play position to edit cursor position