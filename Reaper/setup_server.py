from banner import banner

print(banner)
print("- Setup Script for rapier IPC Server for Reaper")

print("""\n
Hey! This script helps set up server. Here's a few notes of things to 
do before continuing:

- Install and have JACK running.
- Install Reaper if it isn't already.
- Configure Reaper to use JACK.
- Set up the Renoise rapier IPC controller XRNX tool.
- Open Reaper if it isn't already open.""")

input("\nPress enter to continue...")

print("""Ok, the first thing we need to do is to link the transport controls (play, pause, record, etc.)
to a midi device which can be accessed by the rapier server to sync them with renoise.

Go to the preferences pane and under the Audio tab select "MIDI Devices". Double click on a MIDI input 
and take note of it's "Device name" and name it's alias name something that is verbose like 
"rapier Transport Controls" or such. Make sure to select "Enable input for control messages".

""")

print("\n-> Opening virtual midi device...")

from rapier_midi_device import device